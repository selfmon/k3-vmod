File checksums can be validated using cksum filename.bin

Change History Follows
-------------------------------------------------
Version 1.20.000 Initial Release
- Initial release shipped in flash
-------------------------------------------------
Version 1.21.041 Released 2020-09-01
Checksum: 211967144 139936 lce-k3-1_2_1_041.bin

- enhancement adding 5 sec heartbeat publish
- enhancement adding left LED to show broker connection and heartbeat
- enhancement adding version output
- fix for duplicate client ids
- fix for auto reconnect if broker down
- fix for latest compiler version
- fix for increasing heap
-------------------------------------------------
Version 1.21.042 Released 2020-09-03
Checksum 560166749 140328 lce-k3-1_2_1_042.bin

- enhancement switched on retain flag (investigating if these are working)
- enhancement added last will message of 'disconnected' to heartbeat topic path
- added workaround for endless loop in mqtt delete/clear request functions
- added conditional statements for each mqtt publish command to stop repeat publications when one fails
-------------------------------------------------
Version 1.21.043 Released 2020-09-03
Checksum 2312796336 140208 lce-k3-1_2_1_043.bin

- change to min string length of mqtt username from 5 to 4

-------------------------------------------------
Version 1.21.044 - Internal only 
Version 1.21.045 - Internal only 
-------------------------------------------------
Version 1.21.046 Released 2020-09-09
Checksum 4186535176 141264 lce-k3-1_2_1_046.bin

- added watchdog timer for crash reset 
- added mutex lock for LWIP threads in OS env
- added core temperature output (run at about 60 spec is to 90)
- disabled watchdog in firmware cycle

Note that the manual currently shows the wrong pins for
the settings default and bootloader force. The correct
pins are pins 8 + 10 and not nine and ten. I will update
the manual to correct this.
-------------------------------------------------
Version 1.21.047 Released 2020-09-10
Checksum 3962623175 140464 lce-k3-1_2_1_047.bin

- fix for broker offline for any reason

-------------------------------------------------
Version 1.21.048 Released 2020-09-28
Checksum 1800228916 142312 lce-k3-1_2_1_048.bin

- Added menu option for module bus selection for Dimension 96 and above multi-bus panels
- Added option to enable RIO0 and RIO1 for Dimension panels - module default set to disabled for these addresses
- Changed module default keypad to address 19
- Added read-only option for existing physical RIO modules
- Fixed issue where module was enabling the RS485 driver chip when other physical devices may be communicating (this may be the reason that the very old keypad had an issue)
- Updated manual draft7 adding information on module LED states, new menu items and warning about setting read only for existing RIO's

CAUTION:  Please review enabled devices in the configuration page, as this update enables read only mode for physical RIO's. To avoid confusion, Zone input blocks will only be published if one input in the block of 8 zones changes state. This is the same for outputs in blocks of 4. For For virtual RIO's, you can drive publication of a block by publishing an OPEN/CLOSE on the RIO. Once published, the retain flag is set, so the broker will retain upon reconnection.
-------------------------------------------------
Version 1.21.049 - Internal only

- Fixed issue with RIO0 and RIO1 outputs cascading to RIO 2 when publishing 

-------------------------------------------------
Version 1.21.050 Released 2020-09-30
Checksum 2592315340 142736 lce-k3-1_2_1_050.bin 

- Changed physical 'read-only' RIO topic path from 'vmod' to 'pmod'. So now selfmon/vmod.xxxxxx/pmod/
- Fixed issue where the module was publishing Ethernet module event information as HTTP keypad line 3
- Added module sub firmware revision in config web page - consistent with the mqtt version topic
- Added correct gateway and netmask details to config web page

CAUTION - If you have used topic paths for external devices in your home automation routines, ensure that you update them from vmod to pmod paths.
-------------------------------------------------
Version 1.21.051 Released 2020-10-01
Checksum 8592942 142784 /lce-k3-1_2_1_051.bin

- Fixed issue with touchcenter causing VMOD zones to go into hysteresis
-------------------------------------------------
Version 1.21.052 Released 2020-10-02
Checksum 2628381777 142784  lce-k3-1_2_1_052.bin

- Fix to timing issue in 1.21.051 sometimes requiring a double publish to change vrio state

-------------------------------------------------
Version 1.21.053 Released 2020-10-05
Checksum 1611537013 144208 lce-k3-1_2_1_053.bin

- Fixed issue with reporting of vrio/prio outputs on both paths
- Implemented virtual and read only physical RF portals (vportal and pportal paths). The vportal gives the capability of set, part set and unset via topic path for a particular user. The pportal read only paths allow you to perform home automation functions with Honeywell compatible devices. This may be where a device button has not been implemented in firmare. An example of this is the light button that allows night set on Flex, but does nothing on Dimension. Please see the updated manual draft release 010 for details.

Note: Please ensure a clean URL when loading the settings page. If upgrading a jump of more than one release, it's advisable to default the module settings after upgrade.

-------------------------------------------------
Version 1.21.054 Released 2020-10-06
Checksum 120419159 144232 lce-k3-1_2_1_054.bin

- Added connection for MQTT broker when no RS485 bus traffic

-------------------------------------------------
Version 1.21.055 Released 2020-10-07
Checksum 3767667554 144232 lce-k3-1_2_1_055.bin

- Corrected issue with broker username limit. Investigating how this issue managed to return...

-------------------------------------------------
Version 1.21.056 Released 2020-10-19
Checksum 1049956933 148192 lce-k3-1_2_1_056.bin

- Fixes to RF portal code
- Updated config menu to split RF portals out

Note: When updating, you will either need to set default settings, or edit the RF portal section to disable/enable/read-only devices after you update and before you exit engineering or scan for new devices

-------------------------------------------------
Version 1.21.057 Internal only 
Version 1.21.058 Internal only 
Version 1.21.059 Internal only 

-------------------------------------------------
Version 1.21.060 Released 2020-11-27
Checksum 664141911 159512 lce-k3-1_2_1_060.bin

- Corrected V2/Alpha path selection
- Added virtual printer module with MQTT path  selfmon/vprinter/log
  Enable menu 51.28 (Online Print) and set 51.29 (Level) to the level of logging you require
- Added E080 option - this accepts RSS in encrypted mode
- Added SelfMon ARC option (MQTT with secure payload)
- Added special engineer 'Dimension Code Reset' module functionality - only enabled on special hardware builds
- Added IP option for SIA integration - this has been disabled internally as requires further work

Notes: 
- The SelfMon intergration is not fully enabled. The system is running heartbeat and load tests initially.
- The GX app will connect to the module on Dimension panels, but the app in general appears buggy on Dimension.
- The RF module paths still only provide decimal results that need to be decoded.
- Leave SelfMon IP addresses at 0 until platform capability is enabled.      
- SIA support not yet enabled

-------------------------------------------------
Version 1.21.061 Released 2020-11-28
Checksum 1807722507 159512 lce-k3-1_2_1_061.bin

- Enabled RF smoke alarm device ID's. DT PIR's ID was already present.

-------------------------------------------------
Version 1.21.062 Released 2020-12-16
Checksum 4274816029 159512 lce-k3-1_2_1_062.bin

- Changed RF device serialno bitmask from 0x0F to 0x7F to only
  mask the top bit from the serialno

-------------------------------------------------
Version 1.21.063 Released 2020-12-17
Checksum 4023437606 159504 lce-k3-1_2_1_063.bin

- Reduced RF payload to single byte to make translation
  less complex in homeassistant

-------------------------------------------------
Version 1.21.064 Released 2021-01-18
Checksum 84778940 160624 lce-k3-1_2_1_064.bin

- Introduced SIA based zone update. 
  1. Enable RIO's 00 and 01 in read mode. Other RIO's will work too if they are read or enabled.
  2. In the vmod SIA section, enter your Honeywell module IP address.
  3. On your control panel enable SIA4 for the vmod IP address (the DHCP assigned address).
  4. After about 30 seconds, the RIO's will populate under prio/inputs/read
     Now, opening or closing a zone will be reported under that path.

This is quite a heavy change and has had limited testing. So be prepared to revert to v63 if there
are any major bugs.

This firmware is not yet compatible with the secondary bus on Dimension 96 and above. It will 
report RIO's on bus 1 in the prio path

-------------------------------------------------
Version 1.21.065 Released 2021-02-02
Checksum 745625633 160832 lce-k3-1_2_1_065.bin

- Improved implementation of SIA4 zone reading. With better error handling to reconnect
  upon network failure. Also stops the SIA4 lwip errors hanging the http webserver thread.
- Updated manual to include settings guidance for SIA4.

-------------------------------------------------
Version 1.21.066 Released 2021-02-14
Checksum 3917786817 164560 lce-k3-1_2_1_066.bin

- Added group set status in vmod.xxxxxx/sia4/groups topic path. When the SIA4 option is enabled
  in the config screen and with the zones and groups enabled, this option will populate
  the path after around 20 seconds.  Note that Z,Z,G Etc. means that the polling sequence will
  request zones, zones again, then groups. This allows to reduce the number of calls for group status.
- Added polling rate adjustment to config screen. With the Flex panels, the polling rate can
  be very quick, but generally 1s poll is fine.  With Dimension, this is best at 1.5 to 2 seconds,
  but you need to test that this is quick enough for your home automation latency requirements.

Note that a poll interval change (after setting initially) requires a disconnect and reconnect to 
the E080/A083 module. This can be achieved by changing the SIA4 IP to 0.0.0.0 and then setting to
the correct module IP. Or by module reboot or Ethernet disconnect/reconnect. 

-------------------------------------------------
Version 1.21.067 Released 2021-03-10  
Checksum 433253355 165096 lce-k3-1_2_1_067.bin

- Corrections for G2 control panel keypad control in MQTT and App.
- Added G2 panel type in the 'Module Bus' pulldown.
  Note: Selfmon event reporting with E080 emulation will be enabled in a future release

-------------------------------------------------
Version 1.21.068 Released 2021-03-20  
Checksum 1888571432 165592 lce-k3-1_2_1_068.bin

- Addition of SelfMon event reporting when E080 emulation is enabled. 
- Fix to enable port 10001 without the need for a module reboot.

-------------------------------------------------
Version 1.21.069 - Internal release only
Version 1.21.070 - Internal release only

-------------------------------------------------
Version 1.21.071 Released 2021-03-31  
Checksum 1078919784 165616 lce-k3-1_2_1_071.bin

- Change to default mqtt server and usernames to empty values. Where empty
  values result in no connection attempts being made. If not using a local
  mqtt server, it is advisable to set set defaults or set IP to 0.0.0.0

- Reduced stack based allocation which was causing CPU watchdog reset after
  2 days of runtime.

- Additional menu item added for Flex based RS485 receiver using GSM device
  slot. This capability is not fully available yet.

-------------------------------------------------
Version 1.21.072 to 73 - Internal release only

-------------------------------------------------
Version 1.21.074 - Released 2021-04-08 
Checksum 2782059955 165496 lce-k3-1_2_1_074.bin

- Improvement to the mechanism for the SelfMon fallback MQTT server

- Increased heap memory for future TLS/SSL support

-------------------------------------------------
Version 1.21.075 - Released 2021-04-17 
Checksum 2278999624 166032 lce-k3-1_2_1_075.bin

- Improvement for G2 panel connection in E080 mode for app and RSS
- E080 configuration options added to disable port 10001 connections
- Further improvements to SelfMon MQTT server connection mechanism 

-------------------------------------------------
Version 1.21.076 to 79 - Internal releases only

-------------------------------------------------
Version 1.21.080 - Released 2021-04-29
Checksum 3271335911 166288 lce-k3-1_2_1_080.bin

- Fix added for situations where certain traffic on port 10001 could cause
  a watchdog reset. This only impacts E080 emulation with port 10001 enabled.
  This would manifest as a reset of the counter in MQTT. If using E080 emulation
  with port 10001, this update should be implemented.

-------------------------------------------------
Version 1.21.081 to 82 - Internal releases only

-------------------------------------------------
Version 1.21.083 - Released 2021-07-19
Checksum 2533058360 172568 lce-k3-1_2_1_083.bin

- Update to add static IP address option.
- Added option to change SIA remote code from default of 543210. Must still be 6 digit code.
- Fix added to RF portal addresses - remote commands work on all addresses now.
- Option to set time using XTD SIA command for Flex and Dimension. This also works for emulated
  E080 if the SIA4 reporting method is selected in the panel menu.
- Option to add printer based reporting to SelfMon for Flex V3 panels - no A083 hangs this way
  The Flex Bus 1 (flex print to selfmon) bus type, printer enable and selfmon account number must 
  be selected/populated for this to work. 
- Modified SelfMon reporting to enable granular heartbeat - autotest is not required for line path
  monitoring when using the VMOD and E080 emulation.
- Other non disclosable changes for partner customers release version.

-------------------------------------------------
Version 1.21.084 - Released 2021-08-13
Checksum 675236649 172568 lce-k3-1_2_1_084.bin

- Fix for G2 series where outputs were not being reflected in MQTT. 
  G2 panel bus type must be selected.
  Note that virtual printer is not available on G2 series, as it is not a bus device on
  the G2 platform and the non bus printer does not support logging.

-------------------------------------------------
XXXXX DO NOT USE THIS VERSION - POSSIBLE DRIVER ISSUE XXXXX
//
//Version 1.21.085 - Released 2021-09-01
//Checksum 908787791 175368 lce-k3-1_2_1_085.bin
//
//- Added RIO devices for addresses 13 to 15 supporting 112 zones, 56 outputs and 256 links from bus 1.
//- Added newer IRP8M devices to RF devices 
//
//Note: There's a debug version of 085 firmware (085d) in the debug folder. This version provides
//      a telnet port on 5000.  If you telnet to your vmod address on port 5000, then keep pressing
//      the return key, you will receive payload bytes from your 'read only' RF portals. Example: 
//
//telnet 10.226.111.98 5000
//Trying 10.226.111.98...
//Connected to vmod. 
//Escape character is '^]'.
//
//rf05:11FD003951671946182C0D0A66
//rf05:11FD003951671946182C0D0A66
//rf05:11FD003951671906182C0D0A26
//rf05:11FD003951671906182C0D0A26
//
//
-------------------------------------------------
XXXXX DO NOT USE THIS VERSION - POSSIBLE DRIVER ISSUE XXXXX
//Version 1.21.086 - Released 2021-09-05
//2675561098 175392 lce-k3-1_2_1_086.bin
//
//- Corrected bug introduced with version 0.85 where left Ethernet LED had stopped showing status
//  of the MQTT polling cycle.
//

-------------------------------------------------
Version 1.21.087 - Released 2021-10-06
Checksum 2401898197 175664 lce-k3-1_2_1_087.bin

- Fixed issue with driver causing selfmon reporting to fail
- Added prox tag code printout for physical MAX.

Note that although the pmax MQTT path will print the tag ID. Repeated
scanning of tags that are not programmed will cause lockout. A work
around is to enter your code between scanning new tags for their id's.

Note if settting static IP, you must enable an assignment of the same
address under DHCP. The static IP will remain for the duration the module
is powered. You must not assign a static address different to the original
DHCP address.

-------------------------------------------------
Version 1.21.088 - Released 2021-10-13
Checksum 2655903820 175776 lce-k3-1_2_1_088.bin

- Includes option to change broker details from current broker

-------------------------------------------------
Version 1.21.089 - Released 2022-02-11
Checksum 3810724451 175128 lce-k3-1_2_1_089.bin

- Updated to latest LWIP Version 2.1.3
- Implemented timing and thread priority changes to local mqtt
  publishing task

Note that the changes above have been implemented for an issue
observed by 3 customers where the Ethernet interface is unresponsive
after 29 days. 

-------------------------------------------------
Version 1.21.090 - Released 2022-03-08
Checksum 35972084 174040 lce-k3-1_2_1_090.bin

- Removed AutoIP fallback causing issues if DHCP server not available.
- Fixed issue with Static IP selection remaining in web interface after
  power cycle. Static now switches back to DHCP on power cycle.


-------------------------------------------------
Version 1.21.091 - Released 2022-04-23
Checksum 3785309347 174144 lce-k3-1_2_1_091.bin

- Added safeguards against spurious internet traffic on port 10001.
- Auto close TCP port 10001 if no traffic after 3 seconds.

-------------------------------------------------
Version 1.21.092 - Released 2022-07-19
Checksum 3429449890 182440 lce-k3-1_2_1_092.bin

- Added support for keypad display by cursor position

-------------------------------------------------
Version 1.21.093 - Released 2022-10-04
Checksum 3289182004 183944 lce-k3-1_2_1_093.bin

- Corrected bug with SIA based zone reporting for higher RIO addresses 
- Added menu options for RIOs in read-only mode (Bus or SIA modes)

-------------------------------------------------
Version 1.21.094 - Released 2022-10-10
Checksum 507173004 183960 lce-k3-1_2_1_094.bin

- Fixed bug with SIA reporting of zone changes being intermittent
  when the zone also had a link programmed.

-------------------------------------------------
Version 1.21.095 - Beta Release 2023-01-24 
Checksum 281086124 187960 lce-k3-1_2_1_095.bin

- Implements fix for port 10001 access screen refresh. Where apps could
  not show a screen update upon logging in. This issue made it look like
  the app was not logged in when it was. Users would then sit and wait
  on the display and the app would time out.

- Implements all zone and group status on Dimension and G3 in SIA mode
  without the need for a Honeywell E080 module.


-------------------------------------------------
Version 1.21.096 - Beta Release 2023-02-08
Checksum 2997869161 189728 lce-k3-1_2_1_096.bin

- Fixed an issue with XTD SIA command via MQTT not working when the
  option for all zones and groups status is selected on the emulated
  E080 vmod menu.

- Implements all zone status on Flex/Flex+ when the Ethernet module is
  set for Galaxy bus mode. The bus option on the vmod needs to be set
  for this along with Ethernet set to enabled with bus polling. Note
  that there is currently an issue with SelfMon based reporting when
  using this feature on Flex - this will be fixed on version 098. 

  Note that the RS485 Galaxy bus mode for Flex is slow. RSS can still
  connect over port 10001 on the vmod, but the traffic transfer speed
  is poor. This option has been provided for those who want home
  automation via MQTT without having to purchase an A083 module.

- Fixed an issue with static IP setting where the vmod was not calling
  the idle task due to a Galaxy processing task never having any delay
  cycles. The idle task is now running, ensuring that the switch to
  static IP address is made.  If you switch to an address that differs
  from the original assigned address, you will need to change to that
  address in your browser upon submitting the change. Any power cycle
  or hard reset of the module will revert back to DHCP.

-------------------------------------------------
Version 1.21.097 - Released 2023-02-09
Checksum 1948347226 189704 lce-k3-1_2_1_097.bin

- Issue with port 10001 login with RSS introduced in version 096 has 
  been resolved.

-------------------------------------------------
